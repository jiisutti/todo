<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Ostoslista (<?= $maara; ?> )</h3>
    <form action="/ostos/lisaa">
        <label>Kuvaus</label>
        <input name="kuvaus">
        <button>Lisää</button>
    </form>
    <ol>
    <?php foreach($ostokset as $ostos): ?>
        <?= $ostos['kuvaus'];?>
    <?php endforeach;?>
    </ol>
</body>
</html>